<?php
?>

<div id="comments">
  <?php if (($node->comment_count)!="0"): ?><!-- if there are NO comments, print the following -->
    <h3 id="comments-header"><span>Comments</span></h3>
  <?php endif; ?>
  <?php print $content; ?>
</div>
