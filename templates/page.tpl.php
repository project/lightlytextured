<?php
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>

<body class="<?php print $body_classes; ?>">

<div id="page-wrap">  
  <div id="header">
    <?php if (!empty($logo)): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
        <img id="logo" class="left" src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>
  
    <?php if (!empty($site_name)): ?>
      <h1 class="left" id="site-name">
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
      </h1>
    <?php endif; ?>
  
    <?php if (!empty($primary_links)): ?>
      <div id="nav" class="right">
        <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
      </div>
    <?php endif; ?>  
  </div><!-- /header-->


  <div id="main-content" class="left">
    <?php if (!empty($before_content)):?>
      <div id="before-content">
        <?php print $before_content; ?>
      </div>
    <?php endif; ?>        
    <div class="content">
      <?php if (!empty($messages)): print $messages; endif; ?>
      <?php if (!empty($help)): print $help; endif; ?>
      <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
      <?php if (!empty($title)): ?><h2 id="page-title"><span><?php print $title; ?></span></h2><?php endif; ?>      
      <?php print $content; ?>
    </div> <!-- /content -->
  </div><!-- main-content -->
  
  <?php if (!empty($right)): ?>
    <div class="aside right">  
      <?php print $right; ?>
    </div>
  <?php endif; ?><!--/aside -->

  <div id="footer" class="clear">
    <div class="section left">
      <?php print $footer_left; ?>
      <?php print $footer_message; ?>
    </div>
  
    <div class="section right">
      <?php print $footer_right; ?>
      <?php print $feed_icons; ?>
    </div>
  </div><!--/footer -->
</div><!-- page-wrap -->
<?php print $closure; ?>
</body>

</html>