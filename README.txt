; $Id:

*Please note, this hte lightlytextured seeks to be functional in the administration section of Drupal; however, you are highly encouraged to use
and administration theme such as seven (http://drupal.org/project/seven) or rootcandy (http://drupal.org/project/rootcandy) and apply it as 
the administration theme at (<your-sites-root>/admin/settings/admin).

-- About --
LightlyTextured is a theme Designed and Developed by Thomas Lattimore. This theme provides a great design that 
implements a two column layout with lightly textured backgrounds and accents. It has been tested in Firefox 3.5+, 
Safari 4, Chrome 4, and Internet Explorer 7 & 8. A great deal of this themes real beauty relies heavily on CSS3 techniques 
including border-radius, text-shadow, and opacity. All these techniques are designed to improve the look and design of 
the theme, without relying on images. Yet, graceful degrade for the less fortunate browsers that do not support CSS3 yet, 
in other words, it doesn't quite look the same in Internet Explorer.


-- Technical Stuff --

-Explanation of file structure
 --theme root
    -- images - contains all the images used in the theme       
    -- styles - all stylesheets
       -drupalreset.css - serves to as the file to that simply rips and and overrides the bits of CSS drupal generates 
       -style.css - basic theme design and layout
    -- Templates - all the themes .tpl files
    -favicon.ico - themes default favicion
    -lightlytextured.info - themes meta info
    -logo.png - basic themes logo, it's a gear!
    -README.txt - this file...
    -screenshot.png - screenshot for Drupal theme administration page

-Regions
  -- before content
  -- content
  -- footer left
  -- footer right
  -- sidebar

-Tested Modules

*Placeholder for future releases as testing progresses.

